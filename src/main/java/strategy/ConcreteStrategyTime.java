package strategy;
import logic.*;
import java.util.List;

public class ConcreteStrategyTime implements Strategy {

    @Override
    public void addTask(List<Server> servers, Task t){
        int minTime = 5000;
        Server aux = new Server();
        for(Server s : servers){
            if(s.getWaitingPeriod().get() < minTime){
                minTime = s.getWaitingPeriod().get();
                aux = s;
            }
        }
        aux.addTask(t);
    }
}
