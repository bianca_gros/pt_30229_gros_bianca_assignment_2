package strategy;
import logic.*;
import java.util.List;

public class ConcreteStrategyQueue implements Strategy {
    @Override
    public void addTask(List<Server> servers, Task t){
        int minNoClients = 2000;
        for(Server s : servers){
            if(s.getTasks().size() < minNoClients){
                minNoClients = s.getTasks().size();
            }
        }
        for(Server s : servers){
            if(s.getTasks().size() == minNoClients){
                s.getTasks().add(t);
            }
        }
    }
}
