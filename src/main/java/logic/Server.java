package logic;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private volatile boolean activate;
    private int waitTime = 0;

    public Server(){
        this.waitingPeriod = new AtomicInteger(0);
        this.tasks = new LinkedBlockingQueue<Task>();
        this.activate = false;
    }

    public Server(BlockingQueue<Task> tasks, AtomicInteger waitingPeriod) {
        this.tasks = tasks;
        this.waitingPeriod = waitingPeriod;
    }


    public void myFunction(){

        for(Task t: tasks){
            if(t != tasks.peek()){
                t.incFinishTime();
            }
        }

    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(int waitingPeriod) {
        this.waitingPeriod = new AtomicInteger(waitingPeriod);
    }

    public void addTask(Task newTask){
        tasks.add(newTask);
        this.setWaitingPeriod(waitingPeriod.addAndGet(newTask.getProcessingTime()));
        Thread q = new Thread(this);
        if(activate ==  false){
            q.start();
            activate = true;
        }
    }

    public int getWaitTime(){
        return this.waitTime;
    }

    public String printOwnTasks(){
        String s ="";
        if(!this.getTasks().isEmpty() && this.getTasks().peek().getProcessingTime() != 0){
            for(Task t : this.getTasks()) {
                if (t.getProcessingTime() != 0) {
                    s += t;
                }
            }
        }
        return s;
    }

    public void run(){
        try{
            while(activate){
                Thread.sleep(1000);
                if(tasks.size() > 0){
                    this.myFunction();
                    this.waitingPeriod.decrementAndGet();
                    tasks.peek().setProcessingTime(tasks.peek().getProcessingTime() -  1);
                    if(tasks.peek().getProcessingTime() == 0){

                        this.waitTime += tasks.peek().getFinishTime();
                        tasks.poll();

                    }
                }else{
                    activate = false;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String toString(){
        String s = "";
        for(Task t : this.tasks){
            s += t.toString();
        }
        return s;
    }
    public Queue<Task> getTasks(){
        return this.tasks;
    }


    public double waitingTimeOnQueue(){ // returneaza timpul mediu de asteptare la o coada
        double sum = 0;
        double avg;
        ArrayList<Task> t = new ArrayList<Task>();
        this.tasks.drainTo(t);

        double[] s = new double[this.getTasks().size()];
        for(int i = 0; i < this.getTasks().size(); i++){
            s[i] = t.get(i).getProcessingTime();
        }
        for(int i = 1; i < this.getTasks().size(); i++){
            s[i] += s[i - 1];
        }
        for(int i = 0; i < t.size(); i++){
            sum += s[i];
        }
        avg = sum / this.getTasks().size();
        return avg;
    }
}
