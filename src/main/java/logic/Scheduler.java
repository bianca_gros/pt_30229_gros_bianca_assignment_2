package logic;
import strategy.*;
import strategy.Strategy;
import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> servers;
    private int maxNoServers;
    static final int MAX_TASKS_PER_SERVER = 1000;
    private Strategy strategy;
    public enum SelectionPolicy {SHORTEST_QUEUE, SHORTEST_TIME};

    public Scheduler(int maxNoServers){
        this.maxNoServers = maxNoServers;
        this.servers = new ArrayList<Server>();
        for(int i = 0; i < maxNoServers; i++){
            Server aux = new Server();
            servers.add(aux);
        }
        this.strategy = new ConcreteStrategyTime();
    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy == SelectionPolicy.SHORTEST_QUEUE){
            strategy = new ConcreteStrategyQueue();
        }
        if(policy == SelectionPolicy.SHORTEST_TIME){
            strategy = new ConcreteStrategyTime();
        }
    }

    public void dispatchTask(Task t){
        this.strategy.addTask(this.servers, t);
    }

    public List<Server> getServers() {
        return this.servers;
    }

    public String[] afis(){
        int r = 1;
        String[] ret = new String[100];
        String qEmpty = "";
        String q = "";
        int ok = 0;
        for(Server i: servers){
            if(i.getTasks().isEmpty()){
                qEmpty = "Queue " + (r++) + ": closed!\n";
                ret[ok++] = qEmpty;

            }else{
                q = "Queue " + (r++) + ": " + i.toString() + "\n";
                ret[ok++] = q;
            }
        }
        for(int i = 0; i < maxNoServers; i++)
            System.out.println(ret[i]);
        return ret;

    }
}
