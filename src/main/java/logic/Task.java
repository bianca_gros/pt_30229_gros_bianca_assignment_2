package logic;

public class Task implements Comparable<Task>{
    private int id;
    private int arrivalTime;
    private int processingTime;
    private int finishTime;

    public Task(){

    }

    public Task(int id, int arrivalTime, int processingTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
        this.finishTime = processingTime;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    public int getFinishTime() {
        return finishTime;
    }

    public void incFinishTime() {
        this.finishTime += 1;
    }

    public int getId(){ return this.id; }

    public void setId(int id){ this.id = id; }

    @Override
    public String toString() {
        return "(" + id + ", " + arrivalTime + ", " + processingTime + ")";
    }

    public int compareTo(Task o){

        return this.getArrivalTime() - o.getArrivalTime();
    }

    public void setFinishTime(int processingTime) {

        this.finishTime = processingTime;
    }
}
