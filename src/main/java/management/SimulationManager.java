package management;
import logic.*;
import java.util.*;
import java.io.*;

public class SimulationManager implements Runnable{
    public int numberOfClients;
    public int numberOfServers;
    public int timeLimit;
    public int minArrivalTime;
    public int maxArrivalTime;
    public int minProcessingTime;
    public int maxProcessingTime;
    public volatile boolean flag = true;
    public Scheduler.SelectionPolicy selectionPolicy;
    private Scheduler scheduler;
    private List<Task> generatedTasks;
    private FileWriter fout;

    public SimulationManager(String file_in, String file_out) throws FileNotFoundException, InterruptedException {

        File fin = new File(file_in);
        Scanner sc = new Scanner(fin);

        numberOfClients = Integer.parseInt(sc.nextLine());
        numberOfServers = Integer.parseInt(sc.nextLine());
        timeLimit = Integer.parseInt(sc.nextLine());
        String a  = sc.nextLine();
        String[] token = a.split(",");
        minArrivalTime = Integer.parseInt(token[0].toString());
        maxArrivalTime = Integer.parseInt(token[1].toString());
        a = sc.nextLine();
        token = a.split(",");
        minProcessingTime = Integer.parseInt(token[0].toString());
        maxProcessingTime = Integer.parseInt(token[1].toString());
        generatedTasks = new ArrayList<Task>();
        this.generateNRandomTasks();
        this.scheduler = new Scheduler(numberOfServers);
        for(Task t : this.generatedTasks){
            System.out.println(t.toString());
        }
        try {
            fout = new FileWriter(file_out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateNRandomTasks() throws InterruptedException {
        for(int i = 0; i < this.numberOfClients; i++){
            Task t = new Task();
            t.setId(i + 1);
            Random r = new Random();
            t.setArrivalTime(r.nextInt((maxArrivalTime - minArrivalTime) + 1) + minArrivalTime);
            t.setProcessingTime(r.nextInt((maxProcessingTime - minProcessingTime) + 1) + minProcessingTime);
            t.setFinishTime(t.getProcessingTime());
            generatedTasks.add(t);
        }
        Collections.sort(generatedTasks);
    }
    public String printWaitingTasks(int currentTime){
        String s = "";
        boolean afis = true;
        for(Task t : this.generatedTasks){
            if(afis == true){
                System.out.println("Waiting to be sent to a queue: ");
                s += "Waiting to be sent to a queue: ";
                afis = false;
            }
            if(t.getArrivalTime() != currentTime) {
                System.out.println("(" + t.getId() + ", " + t.getArrivalTime() + ", " + t.getProcessingTime() + ")");
                s += t;
            }
        }
        System.out.println(s);
        return s;
    }

    public void writeFunction(String s){
        try {
            fout.write(s);
            fout.write("\n");
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public void writeQueues(){
        int counter = 0;
        for(Server k: scheduler.getServers()){
            String q = "Queue" + counter + ": ";
            counter++;
            if(!k.getTasks().isEmpty()){
                for(Task t: k.getTasks()){
                    q += t.toString();
                }
            }
            else{
                q += " closed!";
            }
            try {
                fout.write(q);
                fout.write('\n');
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void averageTime(){
        double average = 0;
        int clients = 0;
        int procClient = 0;
        for(Server s: scheduler.getServers()){
            average +=s.getWaitTime();
            clients += s.getTasks().size();
        }
        procClient = numberOfClients - clients;
        double newAvg;
        newAvg = average /(double)procClient;
        String toStr = "Average time: " + newAvg;
        try {
            fout.write(toStr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run(){
        int currentTime = 0, count = 0;
        while(currentTime < timeLimit && flag){
            try{ Thread.sleep(1100); }
            catch (InterruptedException e) { e.printStackTrace(); }
            System.out.println("");
            System.out.println("Time: " + currentTime);
            String aux = "Time: " + currentTime;
            writeFunction(aux);
            int p = 0;
            writeFunction(printWaitingTasks(currentTime));
            while(!generatedTasks.isEmpty() && p < generatedTasks.size()){
                if(generatedTasks.get(p).getArrivalTime() == currentTime){
                    scheduler.dispatchTask(generatedTasks.iterator().next());
                    generatedTasks.remove(p);
                }else{
                    p++;
                }
            }
            writeQueues();
            count = 0;
            if(generatedTasks.isEmpty()){
                for(Server gol : scheduler.getServers()){
                    if(gol.getTasks().isEmpty()){
                        count++;
                    }
                }
                if(count == numberOfServers){
                    this.averageTime();
                    try {
                        fout.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    flag = false;
                }
            }
            currentTime++;
        }

    }

    public static void main(String[] args) throws IOException, InterruptedException {
//        SimulationManager gen = new SimulationManager("in-test-2.txt", "out-test-2.txt");
        SimulationManager gen = new SimulationManager(args[0], args[1]);
        Thread sim = new Thread(gen);
        sim.start();
    }
}
